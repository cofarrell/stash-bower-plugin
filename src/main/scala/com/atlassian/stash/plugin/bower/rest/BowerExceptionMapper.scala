package com.atlassian.stash.plugin.bower.rest

import com.atlassian.stash.rest.exception.UnhandledExceptionMapper
import com.atlassian.stash.rest.exception.UnhandledExceptionMapperHelper
import com.sun.jersey.spi.resource.Singleton

import javax.ws.rs.ext.Provider

@Provider
@Singleton
class BowerExceptionMapper(navBuilder: UnhandledExceptionMapperHelper) extends UnhandledExceptionMapper(navBuilder)
