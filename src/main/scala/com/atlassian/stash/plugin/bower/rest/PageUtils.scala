package com.atlassian.stash.plugin.bower.rest

import com.atlassian.stash.util._
import scala.collection.JavaConverters._

object PageUtils {

  def page[A](limit: Int, start: Int = 0)(f: (PageRequest) => Page[A]): Iterable[A] = new PagedIterable[A](new PageProvider[A] {
    override def get(pageRequest: PageRequest) = f(pageRequest)
  }, new PageRequestImpl(start, limit)).asScala
}
