package com.atlassian.stash.plugin.bower.rest

import com.atlassian.plugins.rest.common.security.AnonymousAllowed
import com.atlassian.stash.plugin.bower.rest.PageUtils.page
import com.atlassian.stash.repository.{RepositorySearchRequest, RepositoryService}
import com.atlassian.stash.rest.util.{ResponseFactory, RestUtils}
import com.atlassian.stash.ssh.api.SshCloneUrlResolver
import com.sun.jersey.spi.resource.Singleton

import javax.ws.rs._
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response.Status
import scala.collection.JavaConverters._

@Path("packages")
@Consumes(Array(MediaType.APPLICATION_JSON))
@Produces(Array(RestUtils.APPLICATION_JSON_UTF8))
@Singleton
@AnonymousAllowed
class BowerResource(repositoryService: RepositoryService,
                    sshCloneUrlResolve: SshCloneUrlResolver) {

  @GET
  def all() = ResponseFactory.ok(searchAll("").asJava).build()

  @GET
  @Path("{name}")
  def get(@PathParam("name") name: String) =
    searchAll(name, limit = 1)
      .headOption
      .map(ResponseFactory.ok(_))
      .getOrElse(ResponseFactory.status(Status.NOT_FOUND)).build

  @GET
  @Path("search/{query}")
  def search(@PathParam("query") query: String) =
    ResponseFactory.ok(searchAll(query).asJava).build()

  // This will find _all_ repositories matching a slug - which isn't ideal, but will work 99% of the time...
  private def searchAll(name: String, limit: Int = 1000) = {
    // Get more results than requested so we can filter out forks
    page(limit * 2)(repositoryService.search(
      new RepositorySearchRequest.Builder().name(name).build(), _
    )).filterNot(_.isFork)
      // The _real_ limit
      .take(limit)
      .map {
      repo => Map(
        "name" -> repo.getName,
        "url" -> sshCloneUrlResolve.getCloneUrl(repo)
      ).asJava
    }
  }

}
